# Functionality

This role installs various tools a Database Administrator to use on a linux system:
- Various stored procedures (only if a MySQL or MariaDB process is detected)
- Scripts that call those stored procedures
- Other scripts that are useful for a database administrator
- Install OS packages that are specified by the user (For example for monitoring solutions)
- Lock these packages from automatic system-wide upgrades (only the DBA toolkit will upgrade them)
- Install configuration files and custom scripts defined by the user
- Remove configuration files and custom scripts defined by the user if they are no longer supposed to be installed (this is tracked in the installation directory (default /root/dba-toolkit), so if you remove the installation directory you remove the track of this

## These stored procedures are available:
- convert_all_tables_storage_engine(target_storage_engine, extra_keep_schema, execute): Convert all tables (except those in system schema's and in 'extra_keep_schema') to the target (for example BLACKHOLE) storage engine
- drop_all_constraints(execute): Drop all foreign keys (constraints) on all schemas
- drop_all_functions(execute): Drop all FUNCTIONs on all schemas
- drop_all_procedures(execute): Drop all PROCEDUREs on all schemas
- drop_all_triggers(execute): Drop all TRIGGERs on all schemas
- drop_table_with_referencing_constraints(schema_name, table_name, execute): Drop a single table including it's incoming referential constraints
- drop_table_wildcard(table_schema_wildcard, table_name_wildcard, minimum_age_seconds, execute): Drop all matching (LIKE) tables that were created more then minimum_age_seconds ago
- encrypt-decrypt-tables(schema, encrypt tables, execute): Encrypt all (comma separated) tables in the list, decrypt all other tables
- refresh_materialized_view(schema, view_name): Materialize the view by (atomically) executing INSERT INTO schema.view_name + \_materialized SELECT * FROM schema.view_name
- rename_database(source_schema, target_schema, execute): Move all tables from one schema to another schema (table per table, not atomically)
- kill_idle_connections(minimum_thread_id, minimum_sleep_time_ms): Kill idle connections to apply dynamic configuration change on settings with SESSION scope. For example kill_idle_connections(@@pseudo_thread_id, 1000); - To kill connections that were idle for at least 1 second.
- mass_optimize_tables(exclude_schema, schema_like, table_like, execute): Optimize all matching tables unless they are in system schema's or in exclude schema. For example, mass_optimize_tables('dba', 'app1', '%', 1) - to optimize all tables in the app1 schema.
- print_all_grants(): Print all users as CREATE USER / GRANT statements
- refresh_materialized_view(schema, view_name): The materelization table is view_name + '_materialized'. The procedures refreshes the materialized view. The procedure will create a _new materialization, swap the current to _old and _new to current in 1 atomic statement, and drops the old materelization.
- rename_database(old_schema, new_schema): Creates new_schema_name if it does not exist, moves all tables from old_schema to new_schema. Does not currently create VIEWs (they are writable in MySQL!) in the old schema for easy migration without downtime, PR's are welcome.

## These scripts are available:
- example-import-non-data-objects-to-test-system.sh: Example script to import all non-data objects into a test system (please modify this script and create the cron job yourself); pull requests are welcome to make this templatable and add a cron job for it
- dump-non-data-objects.sh: Dump all non-data objects (procedures, functions, triggers)
- fix-blackhole-replica.sh: Drop all foreign keys and convert all tables to BLACKHOLE, and START ALL SLAVES
- refresh-materialized-view.sh <schema> <view>: Refresh a materialized view
- innodb-lock-monitor.sh /path/to/storage DATE_FORMAT (date_format ofor example %H.%M)
- rotate-sql-error-log.sh: Rotate the SQL error log plugin. For example to avoid the log containing too old sensitive data.

## Example playbook
```
- name: Installing DBA toolkit
  hosts: databases
  become: 1
  roles:
  - role: dba-toolkit
```

And the lines for your requirements.yaml file:
```
- src: git+https://gitlab.com/de-groot-consultancy-ansible-roles/dba-toolkit.git
  name: dba-toolkit
  version: "beta"
```
(instead of beta, you can specify a tag that is specified. This way you pin your installation to a specific version of the DBA toolkit)

## You can install a cron job by passing ansible variables:
```
dba_toolkit_cron_jobs:
- script: "refresh_materialized_view.sh"
  timing: "17 * * * *" # Optional, default @hourly
  name:"refresh application.very_expensive_view"  # Optional, by default the script name and arguments is used.
  args:
  - "application"
  - "very_expensive_view"
- script: "fix-blackhole-replica.sh"
  timing: "* * * * *" # Optional, default @hourly
- script: "innodb-lock-monitor.sh"
  timing: "* * * * *" # Optional, default @hourly
  args:
  - "/root/{{ dba_toolkit_deploy_path }}/lock_monitoring"
  - "%H.%M"
- script: "rotate-sql-error-log.sh"
  timing: "@daily" # Optional, default @hourly
```
The role will (by default) remove all cron jobs that were previously installed by the role. Optionally parameterrs for cron jobs:
- path: colon (:) separated list of search path, defaults to dba_toolkit_cron_path (/usr/sbin:/usr/sbin:/usr/bin:/sbin:/bin)
- user: unix username that execute the cron job (make sure that user can login with a .my.cnf file and that is has access to the files the dba toolkit deploys)

## You can create new schema's and fill them up with initial data
```
dba_toolkit_install_schema:
 - name: "app1"
   sql: "{{ lookup('file', '/path/on/control/host/app1.sql') }}"
   run: "once"
 - name: "app2"
   sql: "{{ lookup('file', '/path/on/control/host/app2-updates.sql') }}"
   run: "always"
```
`run: once` will run only when the schema is created by the dba toolkit, `run: always` will run every time the automation runs.

## Multiple instances support
We support multiple instances by configuring the defaults file
```
dba_toolkit_mysql_defaults_file: "/etc/my_other_instance.cnf"
```
We need to be able to log in without a password (or a password specified in the defaults file) for the procedures to be installed.
The defaults file is included in the deploy path.

## OS Package deploys
The DBA toolkit by default (`dba_toolkit_os_packages_default`) deploys `percona-toolkit`, `percona-xtrabackup`, `sysstat`, `netstat`, `sharutils`. You can also specify your own packages:
```
dba_toolkit_os_packages:
 - pmm2-client
 - zabbix
 - bc
```
Please note that the role locks the packages for upgrade (Debian and Red Hat systems)
