-- FIXME: This procedure most likely not work on MySQL 8.0 anymore. Recommend to implement variable dba_toolkit_metadata_charset / collation.


DELIMITER ;

DROP PROCEDURE IF EXISTS print_all_grants;

DELIMITER //

CREATE PROCEDURE print_all_grants()
SQL SECURITY INVOKER
BEGIN

DECLARE iteration_complete INT DEFAULT 0;
DECLARE role_name VARCHAR(80);
DECLARE username VARCHAR(80);
DECLARE userhost VARCHAR(60);
DECLARE grant_sql VARCHAR(255);
DECLARE role_cursor CURSOR FOR SELECT user FROM mysql.user WHERE is_role="Y" COLLATE utf8mb4_general_ci ORDER BY user COLLATE utf8_general_ci ASC;
DECLARE user_cursor CURSOR FOR SELECT user, host FROM mysql.user WHERE is_role="N" COLLATE utf8mb4_general_ci  ORDER BY user COLLATE utf8_general_ci ASC, host COLLATE utf8_general_ci ASC, password COLLATE utf8mb4_general_ci ASC;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET iteration_complete=1;

SELECT @@sql_mode INTO @old_sql_mode;
SET SESSION sql_mode=REPLACE(@old_sql_mode, 'NO_AUTO_CREATE_USER', '');

OPEN role_cursor;
role_cursor_loop: LOOP
FETCH role_cursor INTO role_name;
IF iteration_complete THEN
LEAVE role_cursor_loop;
END IF;
SELECT CONCAT("SHOW GRANTS FOR `", role_name, "`") INTO grant_sql;
PREPARE grant_query FROM grant_sql;
EXECUTE grant_query;
END LOOP;
CLOSE role_cursor;
SELECT 0 INTO iteration_complete;
OPEN user_cursor;
user_cursor_loop: LOOP
FETCH user_cursor INTO username, userhost;
IF iteration_complete THEN
LEAVE user_cursor_loop;
END IF;
SELECT CONCAT("SHOW GRANTS FOR `", username, "`@`", userhost, "`") INTO grant_sql;
PREPARE grant_query FROM grant_sql;
EXECUTE grant_query;
END LOOP;
CLOSE user_cursor;

SET SESSION sql_mode=@old_sql_mode;

END;
//
