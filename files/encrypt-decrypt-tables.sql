-- This procedure is MariaDB only, so most likely there is no need to implement the template variable dba_toolkit_metadata_charset / collation.
DELIMITER ;

DROP FUNCTION IF EXISTS _get_next_encrypt_table;
DROP FUNCTION IF EXISTS _get_next_decrypt_table;
DROP PROCEDURE IF EXISTS encrypt_decrypt_tables;

DELIMITER //

CREATE FUNCTION `_get_next_encrypt_table`(_schema VARCHAR(64), _include_tables VARCHAR(16000), _sequence_number INT) RETURNS CHAR(255) CHARSET latin1
    DETERMINISTIC
RETURN (SELECT CONCAT("ALTER TABLE `", TABLE_SCHEMA, "`.`", TABLE_NAME, "` ENCRYPTED=Yes") AS alter_table_statement  FROM information_schema.tables WHERE TABLE_SCHEMA = _schema AND INSTR(CONCAT(',', _include_tables, ','), CONCAT(',', TABLE_NAME, ',')) AND CREATE_OPTIONS NOT LIKE "`ENCRYPTED`=yes" LIMIT _sequence_number, 1)
//
CREATE FUNCTION `_get_next_decrypt_table`(_schema VARCHAR(64), _exclude_tables VARCHAR(16000), _sequence_number INT) RETURNS CHAR(255) CHARSET latin1
    DETERMINISTIC
RETURN (SELECT CONCAT("ALTER TABLE `", REPLACE(NAME, "/", "`.`"), "` ENCRYPTED=No") AS alter_table_statement  FROM information_schema.INNODB_TABLESPACES_ENCRYPTION WHERE ENCRYPTION_SCHEME=1 AND NAME LIKE CONCAT(_schema, "/%") AND NOT INSTR(CONCAT(",", _exclude_tables, ","), SUBSTR(NAME FROM LOCATE('/', NAME) + 1)) LIMIT _sequence_number, 1)
//


CREATE PROCEDURE encrypt_decrypt_tables(IN _schema VARCHAR(64), IN _encrypted_tables VARCHAR(16000), IN _execute TINYINT)
BEGIN

DECLARE _alter_table_query VARCHAR(255);
DECLARE _table_sequence INT;

SELECT "Changing character set to utf8mb4 for information_schema query to work" AS status;
SET CHARSET 'utf8mb4';

SELECT IF(_execute, "Running in EXECUTE mode, altering the tables as requested", "Running in CHECK mode, not altering any tables") AS status;

SET _table_sequence = 0;

_alter_table_loop: LOOP
    SELECT _get_next_encrypt_table(_schema, _encrypted_tables, _table_sequence) INTO _alter_table_query;
    IF (_alter_table_query IS NULL) THEN
        LEAVE _alter_table_loop;
    END IF;
    IF (_execute) THEN
        SELECT CONCAT("Encrypting table: ", _alter_table_query) AS status;
        PREPARE alter_table FROM _alter_table_query;
        EXECUTE alter_table;
    ELSE
        SELECT CONCAT("Not encrypting table: ", _alter_table_query) AS status;
        SET _table_sequence = _table_sequence + 1;
    END IF;
END LOOP;

SELECT CONCAT("Done, ENCRYPTed all the tables in ", _schema, ". All tables have been encrypted (assuming we were running in execute mode).") AS status;

SET _table_sequence = 0;

_alter_table_loop: LOOP
    SELECT _get_next_decrypt_table(_schema, _encrypted_tables, _table_sequence) INTO _alter_table_query;
    IF (_alter_table_query IS NULL) THEN
        LEAVE _alter_table_loop;
    END IF;
    IF (_execute) THEN
        SELECT CONCAT("Decrypting table: ", _alter_table_query) AS status;
        PREPARE alter_table FROM _alter_table_query;
        EXECUTE alter_table;
    ELSE
        SELECT CONCAT("Not decrypting table: ", _alter_table_query) AS status;
        SET _table_sequence = _table_sequence + 1;
    END IF;
END LOOP;

SELECT CONCAT("Done, DECRYPTed all the tables in ", _schema, ". All tables have been decrypted (assuming we were running in execute mode).") AS status;
END//

DELIMITER ;
