DELIMITER ;

DROP PROCEDURE IF EXISTS kill_idle_connections;

DELIMITER //

CREATE PROCEDURE kill_idle_connections(IN _maximum_thread_id BIGINT, IN _minimum_sleep_ms INT)
SQL SECURITY INVOKER
BEGIN
DECLARE thread_id INT;
DECLARE iteration_complete INT DEFAULT 0;
DECLARE select_cursor CURSOR FOR SELECT id FROM INFORMATION_SCHEMA.PROCESSLIST WHERE Command="Sleep" and id < _maximum_thread_id AND time_ms >= _minimum_sleep_ms AND user NOT IN ("root", "system user", "event_scheduler") ORDER BY time_ms DESC;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET iteration_complete=1;

OPEN select_cursor;
cursor_loop: LOOP
FETCH select_cursor INTO thread_id;
IF iteration_complete THEN
LEAVE cursor_loop;
END IF;
SELECT * FROM information_schema.processlist where id=thread_id;
SELECT CONCAT("Killing query ", thread_id);
KILL thread_id;
END LOOP;
CLOSE select_cursor;

SELECT "Done" AS status;

SELECT COUNT(1) AS num_threads_left FROM information_schema.processlist where id < _maximum_thread_id AND user NOT IN ("root", "system user", "event_scheduler");
SELECT COUNT(1) AS num_sleeping_threads_left FROM information_schema.processlist where id < _maximum_thread_id AND user NOT IN ("root", "system user", "event_scheduler") AND Command="Sleep";

END;
//

DELIMITER ;
