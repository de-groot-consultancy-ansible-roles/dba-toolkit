#!/bin/bash

gtid_domain_id=$(mysql -Be "SHOW GLOBAL VARIABLES LIKE 'gtid_domain_id'"|grep gtid_domain_id |awk '{print $2}')
wsrep_gtid_domain_id=$(mysql -Be "SHOW GLOBAL VARIABLES LIKE 'wsrep_gtid_domain_id'"|grep wsrep_gtid_domain_id |awk '{print $2}')
wsrep_on=$(mysql -Be "SHOW GLOBAL VARIABLES LIKE 'wsrep_on'"|grep wsrep_on|awk '{print $2}')

for single_domain_gtid in $(mysql -Be "SHOW GLOBAL VARIABLES LIKE 'gtid_binlog_pos'"|grep gtid_binlog_pos|awk '{ print $2 }'|tr ',' '\n')
do
	domain_id=$(echo "$single_domain_gtid" | awk -F '-' '{ print $1 }')
	echo "Found domain $domain_id in binlog position, checking if it should be removed"

	if [ "$gtid_domain_id" -eq "$domain_id" ]; then
		echo "Not removing domain $domain_id is this node's local GTID domain id, not removing (use galera-remove-local-gtid-domain.sh if you need to remove this)"
	elif [ "$wsrep_on" == "ON" ] && [ "$wsrep_gtid_domain_id" -eq "$domain_id" ]; then
		echo "Not removing domain $domain_id is this cluster's wsrep_gtid_domain_id"
	else
		echo "Removing $domain_id is to be removed, as it is neither the local or galera domain id"
		./remove-mariadb-gtid-domain.sh $domain_id
	fi
	echo "------------------"
done
