#!/bin/bash

# Write credentials into ~/.my.cnf and safely execute without interaction:
./dump-non-data-objects.sh -hproduction-machine.local.lan > non-data-objects.sql
# Or, without credentials written:
#./dump-non-data-objects.sh -hproduction-machine.local.lan -uroot -p > non-data-objects.sql

# Change the DEFINER of the objects in case the user names are different on the test system:
sed -i 's/DEFINER=`[a-zA-Z0-9\-_\.]\+`@`[a-zA-Z0-9\-_\.%]\+`/DEFINER=root@localhost/g' non-data-objects.sql
# WARNING: This is only safe when the SQL SECURITY is set to 'INVOKER'. Otherwise permissions might be bumped.

# Next to that, the schema name might be different:
sed -i 's/prod_application_schema/test_application_schema' non-data-objects.sql
# WARNING: This is only safe when the schema name is unique enough and not used differently in the definitions of the non-data objects.

echo "Replacing done"


mysql -htest-machine.local.lan -e "CALL dba.drop_all_procedures();"
mysql -htest-machine.local.lan -e "CALL dba.drop_all_functions();"
mysql -htest-machine.local.lan -e "CALL dba.drop_all_triggers();"
mysql -htest-machine.local.lan -e "CALL dba.drop_all_events();"


mysql -htest-machine.local.lan < non-data-objects.sql
