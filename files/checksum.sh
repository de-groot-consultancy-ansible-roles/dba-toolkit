#!/bin/bash

# Avoid script to work with undefined variables
set -u
  
# Handle control-c
function sigint {
    echo "User has canceled with control-c."
    # 130 is the standard exit code for SIGIN
    exit 130
}                                          

[[ -f "/etc/redhat-release" ]] \
    && default_my_cnf_file="/etc/my.cnf" \
    || default_my_cnf_file="/etc/mysql/my.cnf"


default_dry_run=""
default_superuser="root"
default_checksumuser="dba_toolkit_checksums"
default_checksumhost="%"
default_checksumschema="dba"
default_extra_args=""

curdate=$(date +"%Y%m%d_%h")
default_checksumtable="checksums_${curdate}"


function usage() {
   echo "Syntax: $0"
   echo "Command parameters:"
   echo "    -C <defaults_cnf_file>          Use 'defaults_cnf_file' for db configuration. If not set, default value of '${default_my_cnf_file}' will be used."
   echo "    -u <superuser>                  Superuser to login with. No other options supported, authsocket login is recommended. Default: ${default_superuser}"
   echo "                                    You can specify a defaults file that contains credentials and a host to log in to."
   echo "    -l <checksum user>              The script will create a user with minimal privileges, a safe password and remove it again when done. Default: ${default_checksumuser}"
   echo "    -s <source host>                Source host for the checksum user. Default: '${default_checksumhost}' (all hosts). Configure this for improved security."
   echo "    -D <checksum table schema>      Schema for checksum table. Default: '${default_checksumschema}'. This schema will not be checksummed as well as system schema's."
   echo "    -t <checksum table name>        Table name for checksum table. Default: ${default_checksumtable}."
   echo "    -e <extra args>                 Extra argument to pass to pt-table-checksum. Please note that specifying --ignore-databases will overwrite the script's argument."
   echo "                                    Notable arguments to pass to pt-table-checksum: --max-lag=5m"
   echo ""
   echo "Script config options:"
   echo "    -h                              Print this Help."
   echo "    -d                              Dry run without changing anything, instead printing the commands that would be executed."
   echo
       
   exit 1
}

while getopts C:u:l:s:D:t:e:hd option; do
  case "$option" in
    C)  my_cnf_file="$OPTARG"
        ;;
    u)  superuser="$OPTARG"
        ;;
    l)  checksumuser="$OPTARG"
        ;;
    s)  checksumhost="$OPTARG"
        ;;
    D)  checksumschema="$OPTARG"
        ;;
    t)  checksumtable="$OPTARG"
        ;;
    e)  extra_args="$OPTARG"
        ;;
    h)  usage
        ;;
    d)  dry_run="echo "
        ;;
    *)  usage
        ;;
  esac
done

my_cnf_file="${my_cnf_file:-$default_my_cnf_file}"
dry_run="${dry_run:-$default_dry_run}"
superuser="${superuser:-$default_superuser}"
checksumuser="${checksumuser:-$default_checksumuser}"
checksumhost="${checksumhost:-$default_checksumhost}"
checksumschema="${checksumschema:-$default_checksumschema}"
extra_args="${extra_args:-$default_extra_args}"
checksumtable="${checksumtable:-$default_checksumtable}"


curdate=$(date +"%Y%m%d_%h")
default_checksumtable="checksums_${curdate}"

my_cnf_file_includes=$(grep -E '^[[:space:]]*!((include)|(includedir))[[:space:]]' ${my_cnf_file} | awk '{print $2}')
my_cnf_grep_target="${my_cnf_file} ${my_cnf_file_includes}"

instance_port=$(egrep '\bport\b' $my_cnf_grep_target |head -n 1|awk -F'=' '{print $2}' | xargs)
instance_socket=$(egrep '\bsocket\b' $my_cnf_grep_target |head -n 1|awk -F'=' '{print $2}' | xargs)

[ "$instance_port" = "" ] && instance_port="3306"
[ "$instance_socket" = "" ] && instance_socket="/var/run/mysqld/mysqld.sock"

echo "port: $instance_port, socket: $instance_socket"

generated_password="$(openssl rand -base64 45 | tr -d '=' | cut -c1-45)"
echo "(Re)creating $checksumuser user - password ${generated_password}"
$dry_run mysql \
    --defaults-file=$my_cnf_file \
    --user=$superuser \
    -e "DROP USER IF EXISTS '${checksumuser}'@'${checksumhost}'; 
        CREATE USER '${checksumuser}'@'${checksumhost}' IDENTIFIED BY '${generated_password}'; 
        GRANT SUPER,PROCESS,SELECT,REPLICATION SLAVE,LOCK TABLES ON *.* TO '${checksumuser}'@'${checksumhost}'; 
        GRANT ALL ON ${checksumschema}.* to '${checksumuser}'@'${checksumhost}';
       "

replica_hosts=$(mysql --defaults-file=$my_cnf_file --user=$superuser -e "SHOW REPLICAS;" | awk 'NR>1 {print $2}')

# Check if there are any replicas
if [ -z "$replica_hosts" ]; then
    echo "No replicas found."
    exit 1
fi

# Loop through each replica host
for replica in $replica_hosts; do
    # Timeout in seconds (1 hour = 3600 seconds)
    timeout=3600
    start_time=$(date +%s)

    echo "Waiting until we can log in to the replica $replica"
    [ "$dry_run" != "" ] && echo "Replica check is skipped in dry-run mode as we did not create the user"

    while [ -z "$dry_run" ]; do
        # Execute the command
        mysql --defaults-file=$my_cnf_file \
            --host=$replica \
            --port=${instance_port} \
            --user=${checksumuser} \
            --password=${generated_password} \
            --execute="SELECT COUNT(*) FROM mysql.user WHERE user='${checksumuser}'" \
            >/dev/null 2>/dev/null
        result=$?

        # Check if the command was successful
        if [ $result -eq 0 ]; then
        echo "User appeared on the replica $replica"
        break
        fi

        # Check if the timeout has been reached
        current_time=$(date +%s)
        elapsed_time=$((current_time - start_time))
        if [ $elapsed_time -ge $timeout ]; then
        echo "Timeout reached. Command failed after 1 hour."
        break
        fi

        # Optional: Sleep for a short period before retrying
        sleep 5  # Adjust the sleep duration as needed
    done
done

echo "Starting checksum"
$dry_run pt-table-checksum \
    --defaults-file=$my_cnf_file \
    --password=${generated_password} \
    --user=$checksumuser \
    --host=localhost \
    --socket=${instance_socket} \
    --port=${instance_port} \
    --replicate=${checksumschema}.${checksumtable} \
    --no-check-binlog-format \
    --ignore-databases="${checksumschema},mysql,sys,information_schema,performance_schema" \
    ${extra_args}

checksum_result="$?"

if [ "$dry_run" != "" ]; then
    echo "Dry run mode enabled, disable dry run to execute the checksum process."
elif [ "${checksum_result}" == "255" ]; then
    echo "Exit code was ${checksum_result}, which means there was a fatal error. Keeping the checksums user for debugging purposes."
else
    echo "Exit code was ${checksum_result}. Dropping checksums user."
    mysql --defaults-file=$my_cnf_file --user=$superuser -e "DROP USER IF EXISTS '{$checksumuser}'@'${checksumhost}';"

    [ "$(( checksum_result & 16 ))" -ne 0 ] && echo "WARNING! There were differences detected. Please fix them before there is data loss on a switchover" >&2
    [ "$(( checksum_result & 32 ))" -ne 0 ] && echo "WARNING! At least 1 chunk was skipped in the process. The checksum might be incomplete." >&2
    [ "$(( checksum_result & 64 ))" -ne 0 ] && echo "WARNING! At least 1 table was skipped in the process. The checksum might be incomplete." >&2

    echo "Checksum completed. Recreating the latest_checksum view for to point monitoring to the newest results."
    mysql \
        --defaults-file=$my_cnf_file \
        --user=$superuser \
        -e "DROP VIEW IF EXISTS ${checksumschema}.latest_checksum; 
            CREATE VIEW ${checksumschema}.latest_checksum AS SELECT * FROM ${checksumschema}.${checksumtable};
           "
fi
