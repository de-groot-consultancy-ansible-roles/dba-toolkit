
DELIMITER ;

DROP FUNCTION IF EXISTS _get_next_drop_constraint;
DROP PROCEDURE IF EXISTS drop_all_constraints;

DELIMITER //

CREATE FUNCTION `_get_next_drop_constraint`(_sequence_number INT) RETURNS char(255) CHARSET latin1
    DETERMINISTIC
RETURN (SELECT CONCAT("ALTER TABLE `", constraint_schema, "`.`", table_name, "` DROP CONSTRAINT `", constraint_name, "`") AS alter_statement  FROM information_schema.referential_constraints LIMIT _sequence_number, 1)
//

CREATE PROCEDURE drop_all_constraints(IN _execute TINYINT)
BEGIN
DECLARE _constraint_sequence INT;
DECLARE _alter_query VARCHAR(255);

SELECT IF(_execute, "Running in EXECUTE mode, dropping all constraints as requested", "Running in CHECK mode, not dropping any constraints") AS status;

SET _constraint_sequence = 0;

_alter_loop: LOOP
    SELECT _get_next_drop_constraint(_constraint_sequence) INTO _alter_query;
    IF (_alter_query IS NULL) THEN
        LEAVE _alter_loop;
    END IF;
    IF (_execute) THEN
        SELECT CONCAT("Dropping constraint: ", _alter_query) AS status;
        PREPARE drop_constraint FROM _alter_query;
        EXECUTE drop_constraint;
    ELSE
        SELECT CONCAT("Not droppping constraint: ", _alter_query) AS status;
        SET _constraint_sequence = _constraint_sequence + 1;
    END IF;
END LOOP;

SELECT "Done" AS status;

END;
//
