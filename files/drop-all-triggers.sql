DELIMITER ;

DROP FUNCTION IF EXISTS _get_next_drop_trigger;
DROP PROCEDURE IF EXISTS drop_all_triggers;


DELIMITER //

CREATE FUNCTION `_get_next_drop_trigger`(_sequence_number INT) RETURNS CHAR(255) CHARSET latin1
    DETERMINISTIC
RETURN (SELECT CONCAT("DROP TRIGGER `", TRIGGER_SCHEMA, "`.`", TRIGGER_NAME, "`") AS drop_statement FROM information_schema.TRIGGERS WHERE TRIGGER_SCHEMA NOT IN ("mysql", "information_schema", "performance_schema", "sys", DATABASE()) LIMIT _sequence_number, 1)
//

CREATE PROCEDURE drop_all_triggers(IN _execute TINYINT)
BEGIN

DECLARE _drop_query VARCHAR(255);
DECLARE _drop_sequence INT;

SELECT IF(_execute, "Running in EXECUTE mode, dropping the triggers as requested", "Running in CHECK mode, not dropping the triggers") AS status;

SET _drop_sequence = 0;
_drop_loop: LOOP
    SELECT _get_next_drop_trigger(_drop_sequence) INTO _drop_query;
    IF(_drop_query IS NULL) THEN
        LEAVE _drop_loop;
    END IF;
    IF (_execute) THEN
        SELECT CONCAT("Unfortunately, dropping triggers is not yet supported from a stored procedure. See https://jira.mariadb.org/browse/MDEV-28997 - for now execute the command yourself: \n\\c", _drop_query, ";\n") AS status;
        /*PREPARE drop_trigger FROM _drop_query;*/
        /*EXECUTE drop_trigger;*/
        SET _drop_sequence = _drop_sequence + 1;
    ELSE
        SELECT CONCAT("Not droppping trigger: ", _drop_query) AS status;
        SET _drop_sequence = _drop_sequence + 1;
    END IF;
END LOOP;

END//
